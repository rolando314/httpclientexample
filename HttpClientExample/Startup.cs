﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HttpClientExample.Startup))]
namespace HttpClientExample
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
