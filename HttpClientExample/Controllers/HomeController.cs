﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace HttpClientExample.Controllers {
    public class HomeController : Controller {
        public ActionResult Index() {
            ViewBag.Title = "HttpClient example in ASP.NET MVC";
            return View();
        }

        /// <summary>
        /// Simple test of a REST service.
        /// </summary>
        public async Task<ActionResult> SimpleGetCall() {
            // Local domain: works fine.
            // Remote domains: Timeout in this ASP.NET MVC app. (Work fine in console app.)
            string remoteUriString = "https://httpbin.org/get";
            try {
                // Optionally, get Proxy.
                HttpClientHandler httpClientHandler = buildHttpClientHandler(remoteUriString, false);
                var client = new HttpClient();
                client.BaseAddress = new Uri(remoteUriString);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // GET request.
                var response = await client.GetAsync(remoteUriString).ConfigureAwait(false);
                if (response.IsSuccessStatusCode) {
                    string msg = $"{(int)response.StatusCode}, {response.ReasonPhrase}";
                    ViewBag.Message = "Response: success = " + msg;
                }
                else {
                    string msg = $"{(int)response.StatusCode}, {response.ReasonPhrase}";
                    ViewBag.Messsage = "Response: no success = " + msg;
                }

                return View();
            }
            catch (Exception e) {
                ViewBag.Message = BuildMessage(e);
                return View();
            }
        }

        /// <summary>
        /// Builds a HttpClientHandler, either with or without a proxy.
        /// </summary>
        /// <param name="remoteUriString"></param>
        /// <param name="withProxy"></param>
        /// <returns></returns>
        private HttpClientHandler buildHttpClientHandler(string remoteUriString, bool withProxy) {
            HttpClientHandler ret = null;
            Uri remoteUri = new Uri(remoteUriString);
            IWebProxy proxy = WebRequest.DefaultWebProxy;
            string absProxyUriString = proxy.GetProxy(remoteUri).AbsoluteUri;
            if ((withProxy) && (string.Empty != absProxyUriString)) {
                ret = new HttpClientHandler {
                    Proxy = new WebProxy(absProxyUriString, false),
                    UseProxy = true
                };
            }
            else {
                ret = new HttpClientHandler();
            }
            return ret;
        }

        private string BuildMessage(Exception ex) {
            if (ex == null) {
                throw new ArgumentNullException(nameof(ex));
            }

            var message = ex.Message;
            ex = ex.InnerException;
            while (ex != null) {
                message += $" - {ex.Message}";
                ex = ex.InnerException;
            }

            return message;
        }
    }
}