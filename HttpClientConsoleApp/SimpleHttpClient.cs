﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace HttpClientConsoleApp {
    class SimpleHttpClient {
        public async Task GetDataAsync(string remoteUriString) {
            if (String.IsNullOrEmpty(remoteUriString)) {
                Console.WriteLine("No URI to call.");
                return;
            }

            try {
                // Optionally, get Proxy.
                HttpClientHandler httpClientHandler = buildHttpClientHandler(remoteUriString, false);
                using (var client = new HttpClient(httpClientHandler)) {
                    client.DefaultRequestHeaders.Accept.Clear();

                    // GET request.
                    var response = await client.GetAsync(remoteUriString);
                    string msg = string.Empty;
                    if (response.IsSuccessStatusCode) {
                        Console.WriteLine("Response: success.");
                        msg = $"{(int)response.StatusCode}, {response.ReasonPhrase}";
                    }
                    else {
                        Console.WriteLine("Response: failure.");
                        msg = $"{(int)response.StatusCode}, {response.ReasonPhrase}";
                    }
                    Console.WriteLine("Message: " + msg);

                    return;
                }
            }
            catch (Exception e) {
                string msg = BuildMessage(e);
                Console.WriteLine("Exception: " + msg);
                return;
            }
        }

        /// <summary>
        /// Builds a HttpClientHandler, either with or without a proxy.
        /// </summary>
        /// <param name="remoteUriString"></param>
        /// <param name="withProxy"></param>
        /// <returns></returns>
        private HttpClientHandler buildHttpClientHandler(string remoteUriString, bool withProxy) {
            HttpClientHandler ret = null;
            Uri remoteUri = new Uri(remoteUriString);
            IWebProxy proxy = WebRequest.DefaultWebProxy;
            string absProxyUriString = proxy.GetProxy(remoteUri).AbsoluteUri;
            if ((withProxy) && (string.Empty != absProxyUriString)) {
                ret = new HttpClientHandler {
                    Proxy = new WebProxy(absProxyUriString, false),
                    UseProxy = true
                };
            }
            else {
                ret = new HttpClientHandler();
            }
            return ret;
        }
        private string BuildMessage(Exception ex) {
            if (ex == null) {
                throw new ArgumentNullException(nameof(ex));
            }

            var message = ex.Message;
            ex = ex.InnerException;
            while (ex != null) {
                message += $" - {ex.Message}";
                ex = ex.InnerException;
            }

            return message;
        }
    }
}
