﻿using System;
using System.Threading.Tasks;

namespace HttpClientConsoleApp {
    class Program {
        static void Main(string[] args) {
            MainAsync(args).GetAwaiter().GetResult();
        }

        static async Task MainAsync(string[] argv) {
            string remoteUri = "https://httpbin.org/get";
            Console.WriteLine("GET access to '" + remoteUri + "'.");
            Console.WriteLine("Press 'Enter' to start.");
            Console.ReadLine();

            SimpleHttpClient shc = new SimpleHttpClient();
            await shc.GetDataAsync(remoteUri);

            Console.WriteLine("Done. Press 'Enter' to close.");
            Console.ReadLine();
        }
    }
}
