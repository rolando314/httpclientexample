# README #

This is just an example application showing the use (and possible trouble) of HttpClient. This is connected to the StackOverflow issue at https://stackoverflow.com/questions/46844414/remote-rest-service-calls-in-an-asp-net-mvc-backend

## Use ##

Start the 'HttpClientWebApp' project (F5) and hit the 'Retrieve GET result' link. This eventually results in a (timeout) error when trying to make a simple HTTP GET call.

The same code works fine in the 'HttpClientConsoleApp'.
